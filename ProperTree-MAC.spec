# -*- mode: python ; coding: utf-8 -*-

block_cipher = None
py_files = [
    'ProperTree.py',
    'Scripts/plistwindow.py',
    'Scripts/plist.py',
    'Scripts/run.py',
    'Scripts/utils.py',
]

add_files = [
    ('Scripts/*.*', 'Scripts'),
]

a = Analysis(py_files,
             pathex=['/Users/hp/ProperTree-CN'],
             binaries=[],
             datas=add_files,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='ProperTree',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          icon='logo.ico' )
